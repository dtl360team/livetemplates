
public class ${NAME} {


    /*Example Model*/
    private ${type_attr1} ${name_attr1};
    private ${type_attr2} ${name_attr2};
    
    /* Add to Setters and Getters
     * 1) On the Code menu, click Generate Alt+Insert .
     * 2) In the Generate popup, click one of the following: Getter to generate accessor methods for getting the current values of class fields. ...
     * 3) Select the fields to generate getters or setters for and click OK.
    */

    /*Constructor*/
    public ${NAME}(${type_attr1} ${name_attr1}, ${type_attr2} ${name_attr2}) {
        this.${name_attr1} = ${name_attr1};
        this.${name_attr2} = ${name_attr2};      
    }
  }