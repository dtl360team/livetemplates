import org.springframework.stereotype.Service;


@Service
public class ${NAME} {
 
    private NameService nameService;
 
    @Autowired
    public ${NAME}(NameService nameService) {
        this.nameService = nameService;
    }
 
    public String getUserName(String id) {
        return nameService.getUserName(id);
    }
}