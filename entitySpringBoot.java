@javax.persistence.Entity
class $entity$ {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue
    private java.lang.Long id;
    private java.lang.String name;

    public $entity$() {}

    public $entity$(String name) {
        this.name = name;
    }

    public java.lang.Long getId() {
        return id;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "$entity${" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}